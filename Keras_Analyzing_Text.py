
import sklearn
import nltk
import keras
keras.__version__

from keras.preprocessing.text import Tokenizer
tokenizer = Tokenizer()
trump1 = "How low has President Obama gone to tapp my phones during the very sacred election process. This is Nixon/Watergate. Obama bad (or sick) guy! Sad"
trump2 = "Our wonderful new Healthcare Bill is now out for review and negotiation. ObamaCare is a complete and total disaster - is imploding fast! Sad"
trump3 = "Don't let the FAKE NEWS tell you that there is big infighting in the Trump Admin. We are getting along great, and getting major things done!"
trump4 = "Russia talk is FAKE NEWS put out by the Dems, and played up by the media, in order to mask the big election defeat and the illegal leaks! Sad"
dalaiLama1 = "The purpose of education is to build a happier society, we need a more holistic approach that promotes the practice of love and compassion."
dalaiLama2 = "Be a kind and compassionate person. This is the inner beauty that is a key factor to making a better world."
dalaiLama3 = "If our goal is a happier, more peaceful world in the future, only education will bring change."
dalaiLama4 = "Love and compassion are important, because they strengthen us. This is a source of hope"
tinyCorpus = [trump1, trump2, trump3, trump4, dalaiLama1, dalaiLama2, dalaiLama3, dalaiLama4]
tinyCorpus

tokenizer.fit_on_texts(tinyCorpus)
tokenizer.word_index
sequences = tokenizer.texts_to_sequences(tinyCorpus)
sequences

word_index = tokenizer.word_index
reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])
decoded_review = ' '.join([reverse_word_index.get(i - 3, '?') for i in sequences[0]])

decoded_review

from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer('english')
stemmer.stem('dogs')

print(stemmer.stem('chasing'))
print(stemmer.stem('chased'))
print(stemmer.stem('chases'))

print(stemmer.stem('everyone'))
print(stemmer.stem('please'))
print(stemmer.stem('president'))
print(stemmer.stem('compassionate'))

from keras.preprocessing.text import text_to_word_sequence

words =map(text_to_word_sequence,tinyCorpus)

stemmed_texts = [ map(stemmer.stem, doc) for doc in words]
stemmed_texts

texts = [" ".join(stemmed_words) for stemmed_words in stemmed_texts]
texts


stokenizer = Tokenizer()
stokenizer.fit_on_texts(tinyCorpus)
sequences = stokenizer.texts_to_sequences(tinyCorpus)
sequences[0]


# ## Tokenization Parameters
# One parameter a tokenizer object takes is the number of words. If we specify this, for example,
# 
#     tokenizer = Tokenizer(num_words = 100)
# 
# the tokenizer will only use the top `num_words` most frequent words in the data.

# ## Extracting parts of a text file
# suppose I have the following important email:


important_email = """
To: Ron Zacharski <ron.zacharski@gmail.com>
From: Susan Williams <desmondwilliams614@yahoo.com>
Reply-To: Susan Williams <deswill0119@yahoo.fr>
Message-ID: <1860373470.1061917.1488479328300@mail.yahoo.com>
Subject: Hello,
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
X-FileName:

Hello,

Greetings. With warm heart I offer my friendship and greetings, and I hope that this mail 
will meets you in good time.

However strange or surprising this contact might seem to you as we have not meet personally 
or had any dealings in the past. I humbly ask that you take due consideration of its 
importance and immense benefit.

My name is Susan Williams from Republic of Sierra-Leone. I have something very important 
that i would like to confide in you please,I have a reasonable amount of money which 
i inherited from my late father (Nine Million Five Hundred thousand United States Dollar}.
US$9.500.000.00.which I want to invest in your country with you and again in a very 
profitable venture.
"""


# And suppose we only want the body of the email (and strip out the header.  We can do that as follows:
#

import urllib.request
def parseOutText(all_text):
    """ given an opened email file f, parse out all text below the
        metadata block at the top
        """
    text_string = ''
    content = all_text.split("X-FileName:")
    words = ""
    if len(content) > 1:
        text_string = content[1]
        new_string = " "
        wordlist = text_string.split()
        for word in wordlist:
            word = stemmer.stem(word)
            new_string += " " + str(word)
        
        text_string = new_string
        
    return text_string

## This part just tests the code
target_url = 'http://zacharski.org/files/courses/cs370/important_email.txt'
openfile = urllib.request.urlopen(target_url)
data = openfile.read()
text = data.decode('utf-8')

new_text = parseOutText(text)

print(text)
print(new_text)

import urllib
url = "http://zoo.cs.yale.edu/classes/cs458/lectures/sklearn/ud/ud120-projects-master/enron_mail_20150507.tgz"
urllib.request.urlretrieve(url, filename="../enron_mail_20150507.tgz") 
print ("download complete!")

import tarfile
import os
#os.chdir("..")
tfile = tarfile.open("enron_mail_20150507.tgz", "r:gz")
tfile.extractall(".")
#os.chdir("notebooks")
print ("you're ready to go!")

target_url = 'http://zacharski.org/files/courses/cs370/from_sara.txt'
openfile = urllib.request.urlopen(target_url)
data = openfile.read()
from_sara = data.decode('utf-8').split('\n')


target_url = 'http://zacharski.org/files/courses/cs370/from_chris.txt'
openfile = urllib.request.urlopen(target_url)
data = openfile.read()
from_chris = data.decode('utf-8').split('\n')

import os
import pickle
import re
import sys
import string # raz




from_data = []
word_data = []

temp_counter = 0


for name, from_person in [("sara", from_sara), ("chris", from_chris)]:
    print('PROCESSING  ' + name)
    temp_counter = 0
    for path in from_person:
        ### only look at first 200 emails when developing
        ### once everything is working, remove this line to run over full dataset
        temp_counter += 1
        if temp_counter < 200:   # raz
            temp_counter += 1
            if path != '':
                path = os.path.join('/Users/raz/Code/deep-learning/', path)
                #print(path)
                email = open(path, "r")
                all_text = email.read()
                email.close()
                ### use parseOutText to extract the text from the opened email
                

                ### use str.replace() to remove any instances of the words
                ### ["sara", "shackleton", "chris", "germani"]
                
                ### append the text to word_data

                
                ### - if the email is from Sara, append 0 (zero) to from_data, or append a 1 if Chris wrote the email.


                

print ("emails processed")

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=42)

