# Keras Modeling

For both of these projects, we taught a Keras model to identify aspects of a situation based on the words used in it. Credit goes to Ron Zacharski for the original python notebooks which presented the problem / guidelines for each of our programs. 

They've since been adapted to regular python files, and function the same.

## Keras Movie Reviews

In this program we built a Keras model that would identify whether a movie review was positive or negative based on the words used in it.

We used several dense layers, text stemming, and binary cross entropy to reach our results. We also were asked to analyz the effect of different activation functions on our models accuracy.

## Keras Analyzing Text

Similar to the above program, but using a different corpus of emails / presidential speeches. We also took a look at the Bag - of - Wors (ngram) approach instead of purely Keras.

## An Intro to Keras

Instead of text, we looked at building a model to identify numbers with varying types of handwriting. Sloppy handwriting is hard to recognize, after all. 

